package com.logmein.testing.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginPage {
	
    private final WebDriver browser;

    By emailLocator = By.id("email");	
    By passwordLocator = By.id("passwd");
    By submitLoginButtonLocator = By.id("SubmitLogin");
    By successFieldLocator = By.className("info-account");
    By womanButtonLocator = By.linkText("Women");
    By additem = By.cssSelector("a.product-name[href='http://automationpractice.com/index.php?id_product=4&controller=product']");
    By cartcheck = By.className("icon-ok");
    By addToCart = By.id("add_to_cart");
    By checkout = By.linkText("Proceed to checkout");
    By plussone = By.className("icon-plus");
    By totalprice = By.id("total_price");
    By sameadress = By.id("addressesAreEquals");
    By checkoutbutton = By.cssSelector("button[class='button btn btn-default button-medium']");
    By checkoutbutton_order = By.cssSelector("button[name='processCarrier']");
    By terms = By.id("cgv");
    By paybycheck = By.cssSelector("a.cheque");
    By confirmorder = By.cssSelector("button[type='submit']");
    By orderfieldlocator = By.cssSelector("p.alert");
    
    
    public LoginPage(WebDriver browser) {
        this.browser = browser;
        
        // wait until the page is interactable
        WebElement submitLoginButton = browser.findElement(submitLoginButtonLocator);
        (new WebDriverWait(browser, 5)).until(ExpectedConditions.elementToBeClickable(submitLoginButton));
    }

    public LoginPage typeEmail(String email) {
        browser.findElement(emailLocator).sendKeys(email);
        return this;	
    }

    public LoginPage typePassword(String password) {
        browser.findElement(passwordLocator).sendKeys(password);
        return this;	
    }

    public HomePage submitLogin() {
        browser.findElement(submitLoginButtonLocator).click();
        return new HomePage(browser);	
    }

    public HomePage loginAs(String email, String password) {
    	typeEmail(email);
        typePassword(password);
        return submitLogin();
    }
    
    public boolean isLoginSuccesful() {
        WebElement successField = browser.findElement(successFieldLocator);
        (new WebDriverWait(browser, 5)).until(ExpectedConditions.visibilityOf(successField));

        return successField.getText().equals("Welcome to your account. Here you can manage all of your personal information and orders.");
    }
    
    public LoginPage clickOnWomen() {
        browser.findElement(womanButtonLocator).click();
        return this;	
    }
    
    public LoginPage clickOnDress() {
    	 browser.findElement(additem).click();

    	return this;
    }
    
    public LoginPage clickOnAddToCart()  {
   	 browser.findElement(addToCart).click();

   	return this;
   }

    public boolean isAddSuccesful() {
        WebElement cartCheck = browser.findElement(cartcheck);
        (new WebDriverWait(browser, 5)).until(ExpectedConditions.visibilityOf(cartCheck));

        return cartCheck.getText().equals("Product successfully added to your shopping cart");
    }
    
    
    public LoginPage clickOnCheckOut()  {
      	 browser.findElement(checkout).click();

      	return this;
      }
   
    public LoginPage plussOne()  {
     	 browser.findElement(plussone).click();

     	return this;
     }
    
    public boolean  isTotalCorrect() {
        WebElement totalPrice = browser.findElement(totalprice);
        (new WebDriverWait(browser, 5)).until(ExpectedConditions.visibilityOf(totalPrice));

        return totalPrice.getText().equals("$108.14");
    }
    
    public LoginPage uncheckAdress()  {
    	 browser.findElement(sameadress).click();

    	return this;
    }
    
    public boolean isMasik() {
    	
    	Select dropdown = new Select(browser.findElement(By.id("id_address_invoice")));
    	dropdown.selectByVisibleText("hom");
    	return dropdown.getFirstSelectedOption().equals("hom");
    }
    public LoginPage clickOnCheckOutButton()  {
    	
    	browser.findElement(checkoutbutton).click();

     	return this;
     }
    
    public LoginPage clickOnTerms()  {
    	
    	browser.findElement(terms).click();

     	return this;
     }
    
    public LoginPage clickOnCheckOutButtonOrder()  {
    	
    	browser.findElement(checkoutbutton_order).click();

     	return this;
     }
    public LoginPage clickPayByCheck()  {
    	
    	browser.findElement(paybycheck).click();

     	return this;
     }
    
    public LoginPage clickOnConfirmOrder()  {
    	
    	browser.findElement(confirmorder).click();

     	return this;
     }
    
    public boolean isOrderSuccesful() {
        WebElement orderfield = browser.findElement(orderfieldlocator);
        (new WebDriverWait(browser, 5)).until(ExpectedConditions.visibilityOf(orderfield));

        return orderfield.getText().equals("Your order on My Store is complete.");
    }
    
    
}
