package com.logmein.testing;

import static org.junit.Assert.assertEquals;

import java.nio.file.FileSystems;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.logmein.testing.pageobjects.ContactUsPage;
import com.logmein.testing.pageobjects.HomePage;
import com.logmein.testing.pageobjects.LoginPage;



public class homework {
	
	private WebDriver browser;
	
	@Before
	public void Before() {
		String driverPath = FileSystems.getDefault().getPath("src/test/resources/geckodriver.exe").toString();
		System.setProperty("webdriver.gecko.driver", driverPath);
   	
		FirefoxOptions firefoxOptions = new FirefoxOptions();
		firefoxOptions.setCapability("marionette", true);
		firefoxOptions.setHeadless(false);
		
		browser = new FirefoxDriver(firefoxOptions);
	}

	
	@Test
	public void test() throws InterruptedException {
		
		HomePage homePage = new HomePage(browser);
		homePage.signIn();
		LoginPage loginPage = new LoginPage(browser);
		loginPage.loginAs("gazdaggaborg@gmail.com", "Innocance123");
		loginPage.isLoginSuccesful();
		loginPage.clickOnWomen();
		loginPage.clickOnDress();
		loginPage.clickOnAddToCart();
		loginPage.isAddSuccesful();
		loginPage.clickOnCheckOut();
		loginPage.plussOne();
		loginPage.isTotalCorrect();
		loginPage.clickOnCheckOut();
		loginPage.uncheckAdress();
		
		loginPage.isMasik();
		Thread.sleep(5000);
		loginPage.clickOnCheckOutButton();
		loginPage.clickOnTerms();
		loginPage.clickOnCheckOutButtonOrder();
		loginPage.isTotalCorrect();
		loginPage.clickPayByCheck();
		loginPage.clickOnConfirmOrder();
		loginPage.isOrderSuccesful();
		
	


		
		
	}
	
	
	
	
	
	
	@After
	public void After() {
		browser.quit();
	}
	
	

}
